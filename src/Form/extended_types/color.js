const yup = require("yup");

class ColorSchema extends yup.string {
  constructor() {
    super();

    this.withMutation(() => {
      this.transform((value, originalValue) => {
        if (this.isType(value)) return value;
      });
    });
  }

  _isHex(value) {
    return /^([a-fA-F0-9]{6}|[a-fA-F0-9]{3})$/.test(value);
  }

  _typeCheck(value) {
    return super._typeCheck(value);
  }
}
