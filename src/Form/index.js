const { setup, addSchema } = require("../utils/schemaToForm");

module.exports = class Form {
  constructor(data, schema) {
    const { type, properties, ...validations } = schema;
    const objVal = { noUnknown: true, ...validations };
    let rootValidation = type === "object" ? objVal : validations;
    const form = setup(schema);
    this._data = data;
    this._schema = addSchema(schema.type, rootValidation, form.schema);
  }

  validate(options = {}) {
    const opts = { strict: true, abortEarly: false, ...options };
    return this._schema.validate(this._data, opts);
  }
};
