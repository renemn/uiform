const schema1 = require("./examples/schema1.json");
const data1 = require("./examples/data1.json");
const Form = require("./Form");

(async (data, schema) => {
  try {
    const form = new Form(data, schema);
    const result = await form.validate();
    console.log("SUCCESS!\n", result);
  } catch (e) {
    console.log("FAILED!\n", e.inner || e);
  }
})(data1, schema1);
