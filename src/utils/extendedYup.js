const yup = require("yup");

function length(value, msg) {
  return yup.mixed().test({
    name: "length",
    message: msg || "${path} must contain ${value} characters",
    test: val => val && val.toString().length === length
  });
}

function color(value, msg) {
  return yup.mixed().test({
    name: "color",
    message: msg || "${path} must be valid Hex color code, received ${value}.",
    test: val => /^([a-fA-F0-9]{6}|[a-fA-F0-9]{3})$/.test(val),
    exclusive: true
  });
}

yup.addMethod(yup.mixed, "length", length);
yup.addMethod(yup.string, "color", color);

module.exports = yup;
