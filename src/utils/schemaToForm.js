const dotProp = require("dot-prop");
const yup = require("./extendedYup");

function addSchema(type, validations, value) {
  return Object.keys(validations).reduce((prop, name) => {
    const args = validations[name];
    return prop[name](...(Array.isArray(args) ? args : [args]));
  }, value ? yup[type](value || null) : yup[type]());
}

function setup({ properties: props }) {
  return Object.keys(props).reduce(
    (form, name) => {
      const { type, defaultValue: value, ...validations } = props[name];
      dotProp.set(form.schema, name, addSchema(type, validations, value));
      return form;
    },
    { schema: {} }
  );
}

module.exports = {
  addSchema,
  setup
};
